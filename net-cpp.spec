Name:           net-cpp
Version:	r141
Release:	1%{?dist}
License:	LGPL-3.0
Summary:	A simple yet beautiful networking API for C++11
Url:		https://launchpad.net/net-cpp
Group:		System/Libraries
Source:		https://github.com/abhishek9650/net-cpp/archive/%{name}-%{version}.tar.gz
#Patch:		no-gtest.patch
BuildRequires:	cmake
BuildRequires:	gcc-c++
BuildRequires:	boost-devel, gmock-devel, gtest-devel, json-devel, json-c-devel
BuildRequires:	libcurl-devel
BuildRequires:	python2-simplejson, python2-decorator
BuildRequires:	process-cpp-devel
BuildRequires:	pkg-config
BuildRequires:	graphviz

%description
A simple yet beautiful networking API for C++11.

%package -n libnet-cpp2
Summary:	C++11 library for networking purposes
Group:		System/Libraries

%description -n libnet-cpp2
A simple yet beautiful networking API for C++11.

This package provides shared libraries for net-cpp.

%package devel
Summary:	Development headers for net-cpp
Group:		Development/Libraries/C and C++
Requires:	libnet-cpp2 = %{version}

%description devel 
A simple yet beautiful networking API for C++11.

This package provides development headers for net-cpp.

%prep
%setup -q
truncate -s 0 tests/CMakeLists.txt
#%patch -p1

%build
%cmake
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot} %{?_smp_mflags}

%post -n libnet-cpp2 -p /sbin/ldconfig

%postun -n libnet-cpp2 -p /sbin/ldconfig

%files -n libnet-cpp2
%defattr(-,root,root)
%doc COPYING
%{_libdir}/libnet-cpp.so.2
%{_libdir}/libnet-cpp.so.2.2.0

%files devel
%defattr(-,root,root)
%{_includedir}/core/location.h
%{_includedir}/core/net/error.h
%{_includedir}/core/net/http/client.h
%{_includedir}/core/net/http/content_type.h
%{_includedir}/core/net/http/error.h
%{_includedir}/core/net/http/header.h
%{_includedir}/core/net/http/method.h
%{_includedir}/core/net/http/request.h
%{_includedir}/core/net/http/response.h
%{_includedir}/core/net/http/status.h
%{_includedir}/core/net/http/streaming_client.h
%{_includedir}/core/net/http/streaming_request.h
%{_includedir}/core/net/uri.h
%{_includedir}/core/net/visibility.h
%{_libdir}/libnet-cpp.so
%{_libdir}/pkgconfig/net-cpp.pc

